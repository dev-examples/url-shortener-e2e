wd=`pwd`
rm -rf build package
mkdir build package
git archive -o ./build/`basename $wd`.zip HEAD
dirs=(url-shortener-api url-shortener-client)
for dir in "${dirs[@]}"; do
    cd $wd/$dir
    file="../build/$dir.zip"
    git archive --prefix $dir/ -o $file  HEAD
done

cd $wd/build
for f in *.zip ; do unzip -d ../package -o -u $f ; done
cd $wd/package
python -m zipfile -c ../`basename $wd`.zip .
