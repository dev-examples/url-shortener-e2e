# End-to-end tests 

## Setup

Requires:
- Python (tested on v3.8.1)
- Firefox (tested on v76.0.1)

`bash ./install.sh`

## Running tests

Requires a running demo - see main README.md it parent directory.

`bash ./run.sh`

## Reference

Example robot tests:
https://github.com/robotframework/WebDemo

Selenium API
https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html
