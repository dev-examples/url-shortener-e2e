*** Settings ***
Library           SeleniumLibrary

*** Variables ***
${BASE URL} =  http://korz.ec/

*** Keywords ***
Go To URL Shortener
    Go To  ${HOME URL}
    ${TITLE} =  Get Title
    Should match regexp  ${TITLE}  URL Shortener

Input original URL
    [Arguments]    ${url}
    Input Text  name:originalURL    ${url}

Submit URL
    Click Button  css:form button

Result with short link is visible
    Wait Until Page Contains Element  class:result
    Wait Until Page Contains Element  css:.result a

Result with short link is not visible
    Wait Until Page Does Not Contain Element  class:result

Error message is visible
    Wait Until Page Contains Element  class:error

Short URL redirects to original URL
    [Arguments]    ${shortURL}  ${originalURL}
    Go To  ${shortURL}
    ${location} =  Get Location
    Should Be Equal  ${location}  ${originalURL}

Generate random URL
    [Arguments]    ${size}=20
    ${random} =  Generate Random String  ${size}  [LETTERS][NUMBERS]
    [Return]  ${BASE URL}${random}
    