*** Settings ***
Documentation     E2E tests for URL Shortener web app
Library           SeleniumLibrary
Library           String
Resource          common.robot
Resource          ../common/variables.robot
# Setup & Teardown set to reuse browser window (faster)
Suite Setup       Open Browser  ${HOME URL}  ${BROWSER}
Suite Teardown    Close Browser
Test Teardown     Go To  ${HOME URL}

*** Variables ***
${BASE URL} =  http://korz.ec/

*** Test Cases ***
# Run demo slowly
#     Set Selenium Speed  1    # for slow demo

Create Short URL
    Go To URL Shortener
    ${originalURL} =  Generate random URL
    Input original URL  ${originalURL}
    Submit URL

Invalid original URL
    Go To URL Shortener
    ${originalURL} =  Generate random URL
    Input original URL  ${originalURL}${SPACE}${SPACE}
    Submit URL
    Error message is visible

Too long original URL
    Go To URL Shortener
    ${originalURL} =  Generate random URL  8200
    Input original URL  ${originalURL}
    Submit URL
    Error message is visible
    Wait Until Page Contains  originalURL should not exceed

Create duplicated original URL
    Go To URL Shortener
    ${originalURL} =  Generate random URL
    Input original URL  ${originalURL}
    Submit URL
    Result with short link is visible
    Input original URL  ${originalURL}
    Submit URL
    Result with short link is visible
    Error message is visible

Short URL redirects to original URL    
    Go To URL Shortener
    ${originalURL} =  Generate random URL
    Input original URL  ${originalURL}
    Submit URL
    Result with short link is visible
    ${shortURL} =  Get Element Attribute 	css:.result a 	href
    Short URL redirects to original URL  ${shortURL}   ${originalURL}

*** Keywords ***
