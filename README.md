# Integration demo

To run the backend service and SPA:
`docker-compose up --build`

Go to http://localhost:3000/ in a web browser.

## End-to-end tests

See ./e2e directory.

## Details

See each of the subprojects' README files for more information.
